#!/bin/sh

# Apparently Weechat and users discourage keeping/sharing the text
# configuration files like any other traditional program's dotfiles,
# alledging issues like privacy leaks, overwriting and broken configs.
# Instead they encourage /set, /save commands within Weechat.
#
# I still think these issues can be addressed by just knowing a few
# basic git commands, git filters and hooks (like I do in other configs).
# However at this point I got the impression that Weechat text-file
# configurations are unstable and commands are not; and I do want
# this config to be more modular and hopefully easier/safer to
# install/use for beginners and myself, so I made it a script
# that prints Weechat commands instead, and you can apply them
# like this:
# /exec -sh -oc ./wee.sh
# /save
#
# Inspired by: https://gist.github.com/pascalpoitras/8406501
# Other links:
# https://weechat.org/files/doc/weechat/stable/weechat_user.en.html
# https://weechat.org/files/doc/weechat/stable/weechat_faq.en.html
# https://weechat.org/files/doc/stable/weechat_dev.en.html#color_codes_in_strings

cat_nocom() {
	sed \
		-e '/^[[:space:]]*#/d' \
		-e '/^[[:space:]]*#/d' \
		"$@"
}

set_cmd() {
	cat_nocom \
		-e 's,^,/set '"$1"',' \
		-e '/= /s,,,'
}

secure_data() {
	# This has to be set/run manually
	password="$1" # password for this secured data
	password_liberachat="$2" \
	defaultnicks="$3" \
	autolognick="$4"
	cat <<EOF
/secure passphrase $password
/secure set liberachatpass $password_liberachat
/secure set defaultnicks $defaultnicks
/secure set defaultsasluser $autolognick
EOF
}

weechat_look() {
	username="${1:-"$USER"}"  # change to your network's nick
	set_cmd 'weechat.look.' <<EOF
highlight = *$username*
EOF

	set_cmd 'weechat.look.' <<\EOF
save_config_on_exit = off
bar_more_down = ""
bar_more_up = ""
bar_more_left = "»"
bar_more_right = "«"
buffer_notify_default = message
buffer_time_format = "${color:3}${color:italic}${color:bold}%H${color:9}${color:italic}${color:bold}%M"
color_inactive_message = off
color_inactive_prefix = off
color_inactive_prefix_buffer = off
color_inactive_window = off
day_change_message_1date = "━━▶ ${if:${info:holiday,%d-%m}?${info:holiday,%d-%m}:%a, %d %b %Y} ◀━━"
day_change_message_2dates = "━━▶ ${if:${info:holiday,%%d-%%m}?${info:holiday,%%d-%%m}:%%a, %%d %%b %%Y} (%a, %d %b %Y) ◀━━"
hotlist_add_conditions = "${away} || ${buffer.num_displayed} == 0"
item_buffer_filter = "•"
prefix_align_min = 0
prefix_align_max = 10
prefix_error = "⚠"
prefix_join = "👋 ⇨"
prefix_quit = "💨 ⇦"
prefix_network = "🌐"
prefix_suffix = "│"
read_marker_string = "☑"
separator_horizontal = ""
EOF
}

weechat_color() {
	set_cmd 'weechat.color.' <<\EOF
bar_more = 229
chat_highlight = lightred
chat_highlight_bg = default
chat_nick_colors = "cyan,magenta,green,brown,lightblue,lightcyan,lightmagenta,lightgreen,blue"
chat_prefix_more = 31
chat_prefix_suffix = 31
chat_read_marker = 31
chat_time = 239
chat_delimiters = 31
separator = 31
status_data_highlight = 163
status_data_msg = 229
status_data_private = 121
status_more = 229
status_name = 121
status_name_tls = 121
status_time = white
status_nicklist_count = white
status_number = white
chat_prefix_join = 121
chat_host = 31
EOF
}

irc_look() {
	set_cmd 'irc.look.' <<\EOF
buffer_switch_autojoin = off
buffer_switch_join = off
color_nicks_in_nicklist = on
part_closes_buffer = on
EOF
}

irc_color() {
	# Colors for usernames
	set_cmd 'irc.color.' <<\EOF
message_join = 121
#smart_filter = on
nick_prefixes = "q:lightred;a:lightcyan;o:121;h:lightmagenta;v:229;*:lightblue"
mirc_remap = "1,-1:16;14,-1:240"
EOF
echo '/filter add irc_smart * irc_smart_filter *'
}

aliases() {
	cat_nocom <<\EOF
/alias add clear /buffer clear
/alias add ame allchan -current /me
/alias add amsg allchan -current /msg *
/alias add cq /allpv /buffer close
/alias add znc /quote znc
#/alias add irc_stats /set plugins.var.total_irc_servers 0;/set plugins.var.total_irc_channels 0;/set plugins.var.total_irc_privates 0;/allserv /eval /set plugins.var.total_irc_servers ${calc:${plugins.var.total_irc_servers}+1};/allchan /eval /set plugins.var.total_irc_channels ${calc:${plugins.var.total_irc_channels}+1};/allpv /eval /set plugins.var.total_irc_privates ${calc:${plugins.var.total_irc_privates}+1};/eval I am currently on ${plugins.var.total_irc_servers} server${if:${plugins.var.total_irc_servers}>1?s}, ${plugins.var.total_irc_channels} channel${if:${plugins.var.total_irc_channels}>1?s} and I have ${plugins.var.total_irc_privates} quer${if:${plugins.var.total_irc_privates}>1?ies:y}.;/unset -mask plugins.var.total_irc_*
/alias addcompletion -m close /eval ${if:-m==$*?/allpv }/buffer close
/alias addcompletion %(irc_channel)|%(nick)|%* slap /me slaps $* around a bit with a large trout
/alias addcompletion %(irc_channel)|%(nick)|%* fu /say (╹◡╹)凸 $*
/alias addcompletion %(weechat_commands) multicomm /alias add temp $*;/temp
/alias add trigger_toggle /fset c:${name} =* trigger.trigger.$1-*.enabled;/command -buffer fset.fset * /input send m:*;/fset -toggle
EOF
}

keybind_cmd() {
	cat_nocom \
		-e 's,^,/key bind '"$1"',' \
		-e '/= /s,,,' \
		-e '/"/s,,,g'
}

bindings() {
	keybind_cmd <<\EOF
up = "/input history_global_previous"
down = "/input history_global_next"
ctrl-p = "/input history_global_previous"
ctrl-n = "/input history_global_next"
ctrl-l = "/buffer clear"
meta-k = "/buffer -1"
meta-l = "/buffer +1"
meta-j = "/input move_previous_char"
meta-ñ = "/input move_next_char"
meta-backspace = "/input delete_next_char"
ctrl-K = "/window scroll_up"
ctrl-L = "/window scroll_down"
ctrl-shift-pgdn = "/window page_down"
ctrl-shift-pgup = "/window page_up"
ctrl-I = "/window splith 40"
ctrl-O = "/window splitv 40"
#meta-f,meta-ñ = "/window right"
#meta-f,meta-j = "/window left"
#meta-f,meta-k = "/window up"
#meta-f,meta-l = "/window down"
ctrl-W = "/window close"
EOF
}

weechat_plugin() {
	set_cmd 'weechat.plugin.' <<\EOF
autoload = "*,!spell,!guile,!javascript,!lua,!php,!tcl,!perl,!ruby,!fifo,!xfer,!relay"
EOF
}

logger() {
	echo \
'/set logger.level.irc 4
/set logger.mask.irc %Y/$server/$channel.%m-%d.log'
}

fset() {
	set_cmd 'fset.color.' <<\EOF
line_selected_bg1 = default
name_changed = 229
name_changed_selected = *229
type = 121
type_selected = *121
value = 31
value_changed = 229
value_changed_selected = *229
value_selected = *31
EOF

	set_cmd 'fset.look.' <<\EOF
auto_refresh = "*,!weechat.bar.buflist.size,!plugins.var.buflist.section.*"
EOF

	# bug is set_cmd, it doubles the key
	#set_cmd 'fset.format.option1' <<\EOF
#fset.format.option1 = "${if:${selected_line}?${color:*white}>>:  } ${marked} ${name}  ${type}  ${value2}"
#EOF

	set_cmd 'weechat.bar.fset.' <<\EOF
conditions = "${buffer.full_name} == fset.fset && ${window.win_height} > 7"  
EOF
}

# Bar below chat split
bar_status() {
	cat_nocom <<\EOF
/bar del status
/bar add status root bottom 1 1 ⌚time ,buffer_last_number ,buffer_plugin ,buffer_number+:+buffer_name+(buffer_modes)+{buffer_nicklist_count}+buffer_zoom+buffer_filter,,scroll ,lag ,typing ,completion ,spacer,sys_usage
/bar set status priority 500
/bar set status color_fg white
/bar set status color_delim 31
/bar set status color_bg 234
# this makes the status line not expand below the buffer list, just below chat:
/bar set buflist priority 2000
# No line separator:
/bar set status separator off
# Chat input below status line:
/bar add rootinput root bottom 0 0 [input_prompt]+(away),[input_search],[input_paste],input_text
/bar set rootinput priority 1000
/bar del input
/bar set rootinput name input
EOF
}

# Bar above chat split
bar_title() {
	cat_nocom <<\EOF
/bar del title
/bar add titlenosep window top 1 0  window_number ,buffer_title
/bar set titlenosep priority 500
#/bar set titlenosep conditions ${window.number} == ${if:${plugins.var.control_buffers_window}!=?${plugins.var.control_buffers_window}:2} || ${window.number} == ${if:${plugins.var.control_buffers2_window}!=?${plugins.var.control_buffers2_window}:3} && ${layout[gui_layout_current].name} == main-control-control2
/bar set titlenosep color_fg white
/bar set titlenosep color_delim white
/bar set titlenosep color_bg 234
/bar set titlenosep color_bg_inactive 234
EOF
}

buflist() {
	cat_nocom <<\EOF
/trigger addreplace buflist_tools command "buflist_tools;manage pascalpoitras's custom buflist (https://gist.github.com/pascalpoitras/8406501);resize <buflist_size> <left_size>|? [<right_size>] || hide|show|itoggle|showonly|hideonly <item>|-all [<item>|-all...] || enable|disable|toggle <feature> [<feature>...];       resize: resize the buflist size and the two sections${\n}         hide: hide one or more item(s)${\n}         show: show one or more item(s)${\n}      itoggle: toggle one or more item(s)${\n}     showonly: hide all items except...${\n}     hideonly: show all items except...${\n}       enable: enable a feature that change the behavior of the buflist${\n}      disable: disable a feature that change the behavior of the buflist${\n}       toggle: toggle a feature that change the behavior of the buflist${\n} buflist_size: new size for the buflist${\n}    left_size: size of left section${\n}   right_size: size of right section${\n}         item: an item (buffer_number, fold, indent, nick_prefix, buffer_name, lag, filter, relay_count, extra, hotlist)${\n}         -all: do action on all items${\n}      feature: a feature (indent_tree, real_net_name, show_hidden_buffers)${\n}${\n}The buflist is composed of two sections. The left section is composed of nine items. The buffer number, the fold symbol (servers only), the indentation, the nick prefix (channels only), the buffer name, the lag (servers only), the filter (on some buffer like fset, scripts...), the relay count (for the relay buffer) and extra infos about a buffer. The right section is only composed of hotlist. The two sections can be resized independently.${\n}${\n}Exemples:${\n}  Resize the bar to 20 with the size 15 for left section and 5 for right section:${\n}    /buflist_tools resize 20 15;resize 20 15 || show|hide|itoggle|hideonly|showonly buffer_number|fold|indent|nick_prefix|buffer_name|lag|filter|relay_count|extra|hotlist|-all|%* || enable|disable|toggle indent_tree|real_net_name|show_hidden_buffers|%*"
/trigger set buflist_tools conditions "${tg_argv_eol1} =~ ^resize +[0-9]+ +([0-9]+( +[0-9]+)?|\? +[0-9]+)$ || ${tg_argv_eol1} =~ ^(hide|show|itoggle|showonly|hideonly) +(([[:alnum:]_]+|-all)( +|$))+$ || ${tg_argv_eol1} =~ ^(enable|disable|toggle) +([[:alnum:]_]+( +|$))+$"
/trigger set buflist_tools regex "/.*/${tg_argv1}/my_action /.*/${if:${my_action}==resize?${tg_argv_eol2}}/my_resize_cmds ===^([^ ]+) +([^ ]+)( +([^ ]+))?$===${define:my_buflist_size,${re:1}}${define:my_left_size,${re:2}}${define:my_right_size,${re:4}}/mute /set plugins.var.buflist.section.left.size ${if:${my_left_size}=~[0-9]+?${my_left_size}:${calc:${my_buflist_size}-${my_right_size}}};/mute /set plugins.var.buflist.section.right.size ${if:${my_right_size}=~[0-9]+?${my_right_size}:${calc:${my_buflist_size}-${my_left_size}}};/bar set buflist size ${my_buflist_size}===my_resize_cmds /.*/${if:${my_action}=~^(hide|show|itoggle|showonly|hideonly)$?${tg_argv_eol2}}/my_item_cmds /.*/buffer_number fold indent nick_prefix buffer_name lag filter relay_count extra hotlist/my_items /-all/${my_items}/my_item_cmds /.*/${if:${my_action}=~^(hide|show)only$?${my_items}}/my_hide_or_show_everything ===[^ ]+===/mute /set plugins.var.buflist.item.${re:0} ${if:${my_action}==hideonly};===my_hide_or_show_everything ===[^ ]+===/mute /set plugins.var.buflist.item.${re:0} ${if:${my_action}=~^hide?0:${if:${my_action}=~^show?1:\x5c${if:\x5c${plugins.var.buflist.item.${re:0}${\x5cx7d}?0:1${\x5cx7d}}};===my_item_cmds /.*/${my_hide_or_show_everything}${re:0}/my_item_cmds /.*/${if:${my_action}=~^(enable|disable|toggle)$?${tg_argv_eol2}}/my_look_cmds ===[^ ]+===/mute /set plugins.var.buflist.look.${re:0} ${if:${my_action}==disable?0:${if:${my_action}==enable?1:${if:${plugins.var.buflist.look.${re:0}}!=?0:1}}};===my_look_cmds /.*/${my_resize_cmds}${my_item_cmds}${my_look_cmds}/my_cmds_to_run"
/trigger set buflist_tools command "/command -buffer core.weechat * /eval -s ${my_cmds_to_run}"
# Number of columns of left and right splits (channels and usernames splits):
/buflist_tools resize 20 15
/buflist_tools hideonly extra nick_prefix lag
/buflist_tools enable indent_tree real_net_name
EOF
}

network() {
	cat_nocom <<\EOF
/set irc.network.ban_mask_default "*!*@$host"
#ban_mask_default = "*!$ident@$host"
/set irc.server_default.capabilities "*"
# Networks:
/server add libera irc.libera.chat/6697 -tls
/server add oftc irc.oftc.net/6697
/server add liberator libera75jm6of4wxpxt4aynol3xjmbtxgfyjpu34ss4d7r7q2v5zrpyd.onion
/server add oftctor  oftcnet6xg6roj6d7id4y4cu6dchysacqj2ldgea73qzdagufflqxrid.onion
/set irc.server.libera.tls on
/set irc.server.oftc.tls_cert "%h/ssl/nick.pem"
/set irc.server.oftc.command_delay 5
# Autologin:
#/set irc.server_default.sasl_mechanism PLAIN
#/set irc.server_default.sasl_username ${sec.data.defaultsasluser}
#/set irc.server.libera.sasl_password ${sec.data.liberachatpass}
/set irc.server_default.autojoin_dynamic on
# Nicks without login:
#/set irc.server_default.nicks ${sec.data.defaultnicks}
#/connect -auto
EOF
}

other() {
	cat_nocom <<\EOF
/mouse enable
/set exec.command.shell bash
EOF
#/set irc.server_default.away_check 5
#/set irc.server_default.away_check_max_nicks 25
}

all() {
	#secure_data "passwd" "passwdlib" "nick1,nick2" "nicksasl"
	weechat_look
	weechat_color
	irc_look
	irc_color
	aliases
	bindings
	weechat_plugin
	logger
	bar_status
	bar_title
	#buflist
	fset
	network
	other
}

main() {
	case "$1" in
	(l | -l | look | --look)
		weechat_look
		irc_look
	;;
	(c | -c | color | colour | --color)
		weechat_color
		irc_color
	;;
	(a | -a | alias | aliases)
		aliases
	;;
	(o | -o | other)
		other
	;;
	(logger | --logger)
		logger
	;;
	(p | -p | plugin | --plugin)
		weechat_plugin
	;;
	(bar_status | --bar-status)
		bar_status
	;;
	(bar_title | --bar-title)
		bar_title
	;;
	(k | b | -k | -b | --key | key | keys | bind | bindings )
		bindings
	;;
	(fset)
		fset
	;;
	('')
		all
	;;
	(*)
		"$1"
	esac
}

main "$@"
